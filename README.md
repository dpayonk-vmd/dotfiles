My dotfiles
===========

This is what my ~/.bash_profile looks like.  It integrates this repository by sourcing these files at the beginning

```
# Village Dotfiles Environment
source dotfiles/startup.sh
source dotfiles/aliases.sh
. dotfiles/vmd_environment.sh

# Must explicitly set for use in IntelliJ
export I2_DATABASE_URL="postgres://svc_i2:password@dockerhost/i2_dev"
export SENTRY_URI="'https://177e8da98b3a442c87b11edf3b5aa317:a2336b7cf0b84c6ead643ab102da5aa2@sentry.io/215648"
export WEBAPP_REDIS_URI="redis://dockerhost/0"

# Python Settings for Mac Workstation
. /usr/local/opt/autoenv/activate.sh
export PYENV_ROOT=/usr/local/var/pyenv
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi
if which pyenv-virtualenv-init > /dev/null; then eval "$(pyenv virtualenv-init -)"; fi


# Postgres Settings for Mac Workstation
# from this article http://dba.stackexchange.com/questions/120309/psql-invalid-client-encoding-error-on-os-x-postgresql-9-4-5
export DYLD_LIBRARY_PATH='/usr/local/Cellar/postgresql/9.5.3/lib'


# Java Settings
export JAVA_HOME="$(/usr/libexec/java_home -v 1.8)"


# Golang Settings
export GOPATH="/Users/dpayonk/src/golang"
export GOROOT=/usr/local/go/


# Setup Ruby Environment
#source ~/.rbenv/completions/rbenv.bash


# Terminal Settings
git_branch () { git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'; }

export EDITOR=/usr/bin/nano
export PATH=./bin:~/.rbenv/shims:$PATH:$GOPATH/bin
ulimit -n 8096
HOST='\033[02;36m\]\h'; HOST=' '$HOST
LOCATION=' \033[01;34m\]`pwd | sed "s#\(/[^/]\{1,\}/[^/]\{1,\}/[^/]\{1,\}/\).*\(/[^/]\{1,\}/[^/]\{1,\}\)/\{0,1\}#\1_\2#g"`'
BRANCH=' \033[00;33m\]$(git_branch)\[\033[00m\]\n\$ '
APPENV='\033[01;31m]\t ENVIRONMENT:> ${ENV} < \033[01;32m\]'
PS1=$APPENV$USER$HOST$LOCATION$BRANCH
PS2='\[\033[01;36m\]>'


# Setup iTerm (dev/prod alert) integration
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
```
