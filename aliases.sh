#!/bin/bash

## Custom Aliases
alias s3="aws s3"
alias docker-clean="docker rmi $(docker images --filter "dangling=true" -q --no-trunc)"
