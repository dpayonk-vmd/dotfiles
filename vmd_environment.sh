#!/bin/bash

export I2_DATABASE_URL="postgres://svc_i2:password@dockerhost/i2_dev"
export I2_CONFIG_ROOT="/Users/dpayonk/src/python/i2py/config"
export MAILBOX_QUEUE="dev-villagemd-home-s3"
export MARKET_QUEUE="dev-qa-vmd_s3"
export ORG_DATABASE_URL="postgres://postgres:V!llageMD@dockerhost/dev-il-ord"
export ORGANIZATION_KEY='dev-qa-vmd'
export WEBAPP_REDIS_URI="redis://dockerhost/0"

export FLASK_DEBUG="1"
export ANSIBLE_VAULT_PASSWORD_FILE=./.vault_password.txt

# Local Settings
export POSTGRES_PASSWORD="V!llageMD"
export POSTGRES_USER="postgres"
export POSTGRES_HOST="dockerhost"

export PATH="$PATH:script/:~/bin"
