# Make sure that ur #docker host is active & ready to go, in every shell, no matter what: 

export DOCKER_MACHINE_ACTIVE="development"
export DOCKER_MACHINE_ACTIVE_STATUS=`docker-machine ls | grep -i "$DOCKER_MACHINE_ACTIVE" | grep "Running" | awk '{ print $2}'`
if [ "$DOCKER_MACHINE_ACTIVE_STATUS" != "*" ]; then
  if [ "$DOCKER_MACHINE_ACTIVE_STATUS" != "-" ]; then
    echo "Docker machine $DOCKER_MACHINE_ACTIVE not running. Starting…"
    docker-machine start $DOCKER_MACHINE_ACTIVE
    sleep 3s
    eval "$(docker-machine env $DOCKER_MACHINE_ACTIVE)"
  else
    # echo "Docker machine $DOCKER_MACHINE_ACTIVE not active. Activating…"
    eval "$(docker-machine env $DOCKER_MACHINE_ACTIVE)"
  fi
fi
